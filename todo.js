// If an li element is clicked, toggle the class "done" on the <li>

const todayLiEls = document.getElementsByClassName('today-list')[0];
todayLiEls.addEventListener('click', function(e){e.stopPropagation(); let liDone= e.target;
if (!liDone.getAttribute('class', 'done')){
    liDone.setAttribute('class', 'done');
}else {
    liDone.removeAttribute('class', 'done');
}});

const laterLiEls = document.getElementsByClassName('later-list')[0];
laterLiEls.addEventListener('click', function(e){ let liDone1= e.target;
    if (!liDone1.getAttribute('class', 'done')){
        liDone1.setAttribute('class', 'done');
    }else {
        liDone1.removeAttribute('class', 'done');
    }});

// If a delete link is clicked, delete the li element / remove from the DOM
addDeleteEvents();

function addDeleteEvents(){
const deleteEls = document.getElementsByClassName('delete');
for (let i = 0; i < deleteEls.length; i++)
{
    deleteEls[i].addEventListener('click', function(e) {e.target.parentNode.remove()});
};
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
addMoveEvents()

function addMoveEvents(){
const moveEls = document.getElementsByClassName('move');
for (let i = 0; i < moveEls.length; i++)
{
    moveEls[i].addEventListener('click', function(e)
    {e.stopPropagation();
        let ulMove = e.target.parentNode.parentNode;
        let liMove = e.target.parentNode;
        let aMove = e.target; 

        if (ulMove.classList.contains('today-list')){
            var moveLi = document.getElementsByClassName('later-list')[0];
            moveLi.appendChild(liMove);
            aMove.innerHTML = 'Move to Today';
            aMove.removeAttribute('class', 'move');
            aMove.setAttribute('class', 'move')
            aMove.classList += ('  toToday');
        }
        else
        {
            moveLi = document.getElementsByClassName('today-list')[0];
            moveLi.appendChild(liMove);
            aMove.innerHTML = 'Move to Later';
            aMove.removeAttribute('class', 'move');
            aMove.setAttribute('class', 'move')
            aMove.classList += ('  toLater');

        }
    });
};
}

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addEls = document.getElementsByClassName('add-item');
for (let i = 0; i < addEls.length; i++)
{
    addEls[i].addEventListener('click', function(e)
    {e.stopPropagation(); e.preventDefault();

        const divEL = e.target.parentNode.parentNode;
        const input = this.parentNode.getElementsByTagName('input')[0];
        const text = input.value; // use this text to create a new <li>
        const addEl = document.createElement('li');
        const addElSpan = document.createElement('span');
        const addElA = document.createElement('a');
        const addElA2 = document.createElement('a');

        if (divEL.classList.contains("today")){
            let addUl = document.getElementsByClassName('today-list')[0];
            addUl.appendChild(addEl);
            addEl.appendChild(addElSpan);
            addElSpan.innerHTML = input.value;
            addEl.appendChild(addElA)
            addElA.setAttribute('class', 'move');
            addElA.classList += ('  toLater');
            addElA.innerHTML = 'Move to Later';
            addEl.appendChild(addElA2)
            addElA2.innerHTML = 'Delete';
            addElA2.setAttribute('class', 'delete');

        }
        else {
            let addUl = document.getElementsByClassName('later-list')[0];
            addUl.appendChild(addEl);
            addEl.appendChild(addElSpan);
            addElSpan.innerHTML = input.value;
            addEl.appendChild(addElA)
            addElA.setAttribute('class', 'move');
            addElA.classList += ('  toToday');
            addElA.innerHTML = 'Move to Today';
            addEl.appendChild(addElA2)
            addElA2.innerHTML = 'Delete';
            addElA2.setAttribute('class', 'delete');

        }
        addMoveEvents();
        addDeleteEvents();
        })
}


