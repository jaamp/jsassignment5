// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
const aEl = document.createElement('a');
const aE1TextNode = document.createTextNode('Buy Now');
const m = document.getElementsByTagName('main')[0];
m.appendChild(aEl);
aEl.appendChild(aE1TextNode);
aEl.setAttribute('href','https://www.audi.com/en.html');
aEl.setAttribute('id','cta');

// Access (read) the data-color attribute of the <img>,
// log to the consolea
const imgEl = document.getElementsByTagName('img')[0];
const imgElColor = imgEl.dataset.color;
console.log(imgElColor);

// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
const liEl = document.getElementsByTagName('li')[2];
liEl.setAttribute('class', 'highlight');

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
const pEl = document.getElementsByTagName('p')[0];
m.removeChild(pEl);
